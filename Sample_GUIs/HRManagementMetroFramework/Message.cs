﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRManagementMetroFramework
{
    public partial class Message : MetroFramework.Forms.MetroForm
    {
        MetroFramework.MetroColorStyle color; string m;

        public Message(MetroFramework.MetroColorStyle c, string message)
        {
            color = c;
            m = message;
            InitializeComponent();
        }

        private void Message_Load(object sender, EventArgs e)
        {
            this.Style = color;
            this.Text = m;
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
