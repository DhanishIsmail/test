﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRManagementMetroFramework
{
    class Employee
    {
        public int ID = 0;
        public string Name = "";
        public string NIC = "";
        public string Contact = "";
        public string Expertise = "";
        public int Salary = 0;
        public string Address = "";

        public Employee(int ID, string Name, string NIC, string Contact, string Expertise, int Salary, string Address)
        {
            this.ID = ID;
            this.Name = Name;
            this.NIC = NIC;
            this.Contact = Contact;
            this.Expertise = Expertise;
            this.Salary = Salary;
            this.Address = Address;
        }
    }
}
