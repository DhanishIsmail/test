﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace PaymentMetroFramework
{
    public partial class Payment : MetroFramework.Forms.MetroForm
    {
        private bool CashSelcted = false;
        private bool CardSelected = false;

        public Payment()
        {
            InitializeComponent();
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void CashButton_Click(object sender, EventArgs e)
        {
            this.Style = CashTextBox.Style;
            Transition t = new Transition(new TransitionType_CriticalDamping(2000));
            t.add(CashButton, "Left", (this.Size.Width - 200));
            t.add(CardButton, "Left", (this.Size.Width - 200));
            t.run();

            if (!CashSelcted)
            {
                CashViewDisplay();
            }
            else { CashControlsVisible(true); CardControlVisible(false); }

            CashSelcted = true;
        }

        private void CardButton_Click(object sender, EventArgs e)
        {
            this.Style = CardAmountTxtBx.Style;
            Transition t = new Transition(new TransitionType_CriticalDamping(2000));
            t.add(CardButton, "Left", (this.Size.Width - 200));
            t.add(CashButton, "Left", (this.Size.Width - 200));
            t.run();

            if (!CardSelected)
            {
                CardViewDisplay();
            }
            else { CardControlVisible(true); CashControlsVisible(false); }

            CardSelected = true;
        }

        private void CashControlsVisible(bool visibility)
        {
            CashTextBox.Visible = visibility;
            One.Visible = visibility; Two.Visible = visibility; Three.Visible = visibility;
            Four.Visible = visibility; Five.Visible = visibility; Six.Visible = visibility;
            Seven.Visible = visibility; Eight.Visible = visibility; Nine.Visible = visibility;
            Clear.Visible = visibility; Zero.Visible = visibility; OK.Visible = visibility;
        }

        private void CashViewDisplay()
        {
            CardControlVisible(false);
            CashControlsVisible(true);
            
            Transition t = new Transition(new TransitionType_CriticalDamping(2000));
            t.add(CashTextBox, "Left", (250 - CashTextBox.Left));
            t.add(One, "Left", (250 - One.Left)); t.add(Two, "Left", (309 - One.Left)); t.add(Three, "Left", (364 - One.Left));
            t.add(Four, "Left", (250 - Four.Left)); t.add(Five, "Left", (309 - Four.Left)); t.add(Six, "Left", (364 - Four.Left));
            t.add(Seven, "Left", (250 - Seven.Left)); t.add(Eight, "Left", (309 - Seven.Left)); t.add(Nine, "Left", (364 - Seven.Left));
            t.add(Clear, "Left", (250 - Clear.Left)); t.add(Zero, "Left", (309 - Clear.Left)); t.add(OK, "Left", (364 - Clear.Left));

            t.run();
        }

        private void CardControlVisible(bool visibility)
        {
            CardAmountLbl.Visible = visibility; CardAmountTxtBx.Visible = visibility;
            ExpDateLbl.Visible = visibility; MonthTxtBx.Visible = visibility; YearTxtBx.Visible = visibility;
            CCVLbl.Visible = visibility; CCVTxtBx.Visible = visibility; ConfirmBtn.Visible = visibility;
        }

        private void CardViewDisplay()
        {
            CashControlsVisible(false);
            CardControlVisible(true);

            Transition t = new Transition(new TransitionType_CriticalDamping(2000));
            t.add(CardAmountLbl, "Left", (250 - CardAmountLbl.Left));
            t.add(CardAmountTxtBx, "Left", (250 - CardAmountLbl.Left));
            t.add(ExpDateLbl, "Left", (250 - CardAmountLbl.Left));
            t.add(MonthTxtBx, "Left", (250 - CardAmountLbl.Left));
            t.add(YearTxtBx, "Left", (345 - CardAmountLbl.Left));
            t.add(CCVLbl, "Left", (250 - CardAmountLbl.Left));
            t.add(CCVTxtBx, "Left", (250 - CardAmountLbl.Left));
            t.add(ConfirmBtn, "Left", (292 - CardAmountLbl.Left));

            t.run();
        }

        private void One_Click(object sender, EventArgs e)
        { CashTextBox.Text += "1"; }

        private void Two_Click(object sender, EventArgs e)
        { CashTextBox.Text += "2"; }

        private void Three_Click(object sender, EventArgs e)
        { CashTextBox.Text += "3"; }

        private void Four_Click(object sender, EventArgs e)
        { CashTextBox.Text += "4"; }

        private void Five_Click(object sender, EventArgs e)
        { CashTextBox.Text += "5"; }

        private void Six_Click(object sender, EventArgs e)
        { CashTextBox.Text += "6"; }

        private void Seven_Click(object sender, EventArgs e)
        { CashTextBox.Text += "7"; }

        private void Eight_Click(object sender, EventArgs e)
        { CashTextBox.Text += "8"; }

        private void Nine_Click(object sender, EventArgs e)
        { CashTextBox.Text += "9"; }

        private void Clear_Click(object sender, EventArgs e)
        { try { CashTextBox.Text = CashTextBox.Text.Remove(CashTextBox.Text.Length - 1); } catch(ArgumentOutOfRangeException) { /* skip */ } }

        private void Zero_Click(object sender, EventArgs e)
        { CashTextBox.Text += "0"; }

        private void OK_Click(object sender, EventArgs e)
        {
            Message m = new Message("Your payment has been successfully made!", CashTextBox.Style);
            m.ShowDialog();
            this.Close();
        }

        private void ConfirmBtn_Click(object sender, EventArgs e)
        {
            Message m = new Message("Your payment has been successfully made!", CardButton.Style);
            m.ShowDialog();
            this.Close();
        }
    }
}
