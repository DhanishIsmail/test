﻿namespace PaymentMetroFramework
{
    partial class Message
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OK = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Highlight = true;
            this.OK.Location = new System.Drawing.Point(214, 78);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(73, 40);
            this.OK.TabIndex = 0;
            this.OK.Text = "OK";
            this.OK.Theme = MetroFramework.MetroThemeStyle.Light;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Message
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 149);
            this.ControlBox = false;
            this.Controls.Add(this.OK);
            this.Name = "Message";
            this.Opacity = 0.98D;
            this.Text = "Message";
            this.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center;
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Load += new System.EventHandler(this.Message_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton OK;
    }
}