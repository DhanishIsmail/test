﻿namespace HRManagementMetroFramework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VwEmplyDtlsLbl = new MetroFramework.Controls.MetroLabel();
            this.CloseBtn = new MetroFramework.Controls.MetroButton();
            this.AddEmplyLbl = new MetroFramework.Controls.MetroLabel();
            this.NameLbl = new MetroFramework.Controls.MetroLabel();
            this.NameTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.NICLbl = new MetroFramework.Controls.MetroLabel();
            this.NICTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.ExpertiseLbl = new MetroFramework.Controls.MetroLabel();
            this.ExpertiseTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.ContactLbl = new MetroFramework.Controls.MetroLabel();
            this.ContactTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.SalaryLbl = new MetroFramework.Controls.MetroLabel();
            this.SalaryTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.AddressLbl = new MetroFramework.Controls.MetroLabel();
            this.AddressTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.AddBtn = new MetroFramework.Controls.MetroButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // VwEmplyDtlsLbl
            // 
            this.VwEmplyDtlsLbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VwEmplyDtlsLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.VwEmplyDtlsLbl.Location = new System.Drawing.Point(78, 112);
            this.VwEmplyDtlsLbl.Name = "VwEmplyDtlsLbl";
            this.VwEmplyDtlsLbl.Size = new System.Drawing.Size(225, 33);
            this.VwEmplyDtlsLbl.Style = MetroFramework.MetroColorStyle.Teal;
            this.VwEmplyDtlsLbl.TabIndex = 0;
            this.VwEmplyDtlsLbl.Text = "View Employee Details >";
            this.VwEmplyDtlsLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.VwEmplyDtlsLbl.UseStyleColors = true;
            this.VwEmplyDtlsLbl.Click += new System.EventHandler(this.VwEmplyDtlsLbl_Click);
            this.VwEmplyDtlsLbl.MouseEnter += new System.EventHandler(this.VwEmplyDtlsLbl_MouseEnter);
            this.VwEmplyDtlsLbl.MouseLeave += new System.EventHandler(this.VwEmplyDtlsLbl_MouseLeave);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Highlight = true;
            this.CloseBtn.Location = new System.Drawing.Point(953, 5);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(23, 23);
            this.CloseBtn.Style = MetroFramework.MetroColorStyle.Red;
            this.CloseBtn.TabIndex = 1;
            this.CloseBtn.Text = "X";
            this.CloseBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // AddEmplyLbl
            // 
            this.AddEmplyLbl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddEmplyLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.AddEmplyLbl.Location = new System.Drawing.Point(78, 163);
            this.AddEmplyLbl.Name = "AddEmplyLbl";
            this.AddEmplyLbl.Size = new System.Drawing.Size(225, 33);
            this.AddEmplyLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.AddEmplyLbl.TabIndex = 2;
            this.AddEmplyLbl.Text = "Add New Employee >";
            this.AddEmplyLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.AddEmplyLbl.UseStyleColors = true;
            this.AddEmplyLbl.Click += new System.EventHandler(this.AddEmplyLbl_Click);
            this.AddEmplyLbl.MouseEnter += new System.EventHandler(this.AddEmplyLbl_MouseEnter);
            this.AddEmplyLbl.MouseLeave += new System.EventHandler(this.AddEmplyLbl_MouseLeave);
            // 
            // NameLbl
            // 
            this.NameLbl.AutoSize = true;
            this.NameLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.NameLbl.Location = new System.Drawing.Point(308, 112);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(52, 19);
            this.NameLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.NameLbl.TabIndex = 3;
            this.NameLbl.Text = "Name :";
            this.NameLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.NameLbl.UseStyleColors = true;
            this.NameLbl.Visible = false;
            // 
            // NameTxtBx
            // 
            this.NameTxtBx.Location = new System.Drawing.Point(366, 108);
            this.NameTxtBx.Name = "NameTxtBx";
            this.NameTxtBx.Size = new System.Drawing.Size(362, 23);
            this.NameTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.NameTxtBx.TabIndex = 4;
            this.NameTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.NameTxtBx.UseStyleColors = true;
            this.NameTxtBx.Visible = false;
            // 
            // NICLbl
            // 
            this.NICLbl.AutoSize = true;
            this.NICLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.NICLbl.Location = new System.Drawing.Point(321, 163);
            this.NICLbl.Name = "NICLbl";
            this.NICLbl.Size = new System.Drawing.Size(39, 19);
            this.NICLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.NICLbl.TabIndex = 5;
            this.NICLbl.Text = "NIC :";
            this.NICLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.NICLbl.UseStyleColors = true;
            this.NICLbl.Visible = false;
            // 
            // NICTxtBx
            // 
            this.NICTxtBx.Location = new System.Drawing.Point(366, 159);
            this.NICTxtBx.MaxLength = 10;
            this.NICTxtBx.Name = "NICTxtBx";
            this.NICTxtBx.Size = new System.Drawing.Size(71, 23);
            this.NICTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.NICTxtBx.TabIndex = 6;
            this.NICTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.NICTxtBx.UseStyleColors = true;
            this.NICTxtBx.Visible = false;
            // 
            // ExpertiseLbl
            // 
            this.ExpertiseLbl.AutoSize = true;
            this.ExpertiseLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.ExpertiseLbl.Location = new System.Drawing.Point(459, 163);
            this.ExpertiseLbl.Name = "ExpertiseLbl";
            this.ExpertiseLbl.Size = new System.Drawing.Size(70, 19);
            this.ExpertiseLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.ExpertiseLbl.TabIndex = 7;
            this.ExpertiseLbl.Text = "Expertise :";
            this.ExpertiseLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ExpertiseLbl.UseStyleColors = true;
            this.ExpertiseLbl.Visible = false;
            // 
            // ExpertiseTxtBx
            // 
            this.ExpertiseTxtBx.Location = new System.Drawing.Point(535, 159);
            this.ExpertiseTxtBx.Name = "ExpertiseTxtBx";
            this.ExpertiseTxtBx.Size = new System.Drawing.Size(193, 23);
            this.ExpertiseTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.ExpertiseTxtBx.TabIndex = 8;
            this.ExpertiseTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ExpertiseTxtBx.UseStyleColors = true;
            this.ExpertiseTxtBx.Visible = false;
            // 
            // ContactLbl
            // 
            this.ContactLbl.AutoSize = true;
            this.ContactLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.ContactLbl.Location = new System.Drawing.Point(296, 214);
            this.ContactLbl.Name = "ContactLbl";
            this.ContactLbl.Size = new System.Drawing.Size(64, 19);
            this.ContactLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.ContactLbl.TabIndex = 9;
            this.ContactLbl.Text = "Contact :";
            this.ContactLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ContactLbl.UseStyleColors = true;
            this.ContactLbl.Visible = false;
            // 
            // ContactTxtBx
            // 
            this.ContactTxtBx.Location = new System.Drawing.Point(366, 210);
            this.ContactTxtBx.MaxLength = 10;
            this.ContactTxtBx.Name = "ContactTxtBx";
            this.ContactTxtBx.Size = new System.Drawing.Size(71, 23);
            this.ContactTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.ContactTxtBx.TabIndex = 10;
            this.ContactTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ContactTxtBx.UseStyleColors = true;
            this.ContactTxtBx.Visible = false;
            // 
            // SalaryLbl
            // 
            this.SalaryLbl.AutoSize = true;
            this.SalaryLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.SalaryLbl.Location = new System.Drawing.Point(477, 214);
            this.SalaryLbl.Name = "SalaryLbl";
            this.SalaryLbl.Size = new System.Drawing.Size(52, 19);
            this.SalaryLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.SalaryLbl.TabIndex = 11;
            this.SalaryLbl.Text = "Salary :";
            this.SalaryLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.SalaryLbl.UseStyleColors = true;
            this.SalaryLbl.Visible = false;
            // 
            // SalaryTxtBx
            // 
            this.SalaryTxtBx.Location = new System.Drawing.Point(535, 210);
            this.SalaryTxtBx.Name = "SalaryTxtBx";
            this.SalaryTxtBx.Size = new System.Drawing.Size(193, 23);
            this.SalaryTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.SalaryTxtBx.TabIndex = 12;
            this.SalaryTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.SalaryTxtBx.UseStyleColors = true;
            this.SalaryTxtBx.Visible = false;
            // 
            // AddressLbl
            // 
            this.AddressLbl.AutoSize = true;
            this.AddressLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.AddressLbl.Location = new System.Drawing.Point(295, 265);
            this.AddressLbl.Name = "AddressLbl";
            this.AddressLbl.Size = new System.Drawing.Size(65, 19);
            this.AddressLbl.Style = MetroFramework.MetroColorStyle.Orange;
            this.AddressLbl.TabIndex = 13;
            this.AddressLbl.Text = "Address :";
            this.AddressLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.AddressLbl.UseStyleColors = true;
            this.AddressLbl.Visible = false;
            // 
            // AddressTxtBx
            // 
            this.AddressTxtBx.Location = new System.Drawing.Point(366, 261);
            this.AddressTxtBx.Name = "AddressTxtBx";
            this.AddressTxtBx.Size = new System.Drawing.Size(362, 23);
            this.AddressTxtBx.Style = MetroFramework.MetroColorStyle.Orange;
            this.AddressTxtBx.TabIndex = 14;
            this.AddressTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.AddressTxtBx.UseStyleColors = true;
            this.AddressTxtBx.Visible = false;
            // 
            // AddBtn
            // 
            this.AddBtn.Highlight = true;
            this.AddBtn.Location = new System.Drawing.Point(653, 348);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(75, 28);
            this.AddBtn.Style = MetroFramework.MetroColorStyle.Orange;
            this.AddBtn.TabIndex = 15;
            this.AddBtn.Text = "Add";
            this.AddBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.AddBtn.Visible = false;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(199, 108);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(545, 310);
            this.listView1.TabIndex = 16;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(976, 527);
            this.ControlBox = false;
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.AddressTxtBx);
            this.Controls.Add(this.AddressLbl);
            this.Controls.Add(this.SalaryTxtBx);
            this.Controls.Add(this.SalaryLbl);
            this.Controls.Add(this.ContactTxtBx);
            this.Controls.Add(this.ContactLbl);
            this.Controls.Add(this.ExpertiseTxtBx);
            this.Controls.Add(this.ExpertiseLbl);
            this.Controls.Add(this.NICTxtBx);
            this.Controls.Add(this.NICLbl);
            this.Controls.Add(this.NameTxtBx);
            this.Controls.Add(this.NameLbl);
            this.Controls.Add(this.AddEmplyLbl);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.VwEmplyDtlsLbl);
            this.Name = "Form1";
            this.Opacity = 0.98D;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.Text = "Human Resource Management";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel VwEmplyDtlsLbl;
        private MetroFramework.Controls.MetroButton CloseBtn;
        private MetroFramework.Controls.MetroLabel AddEmplyLbl;
        private MetroFramework.Controls.MetroLabel NameLbl;
        private MetroFramework.Controls.MetroTextBox NameTxtBx;
        private MetroFramework.Controls.MetroLabel NICLbl;
        private MetroFramework.Controls.MetroTextBox NICTxtBx;
        private MetroFramework.Controls.MetroLabel ExpertiseLbl;
        private MetroFramework.Controls.MetroTextBox ExpertiseTxtBx;
        private MetroFramework.Controls.MetroLabel ContactLbl;
        private MetroFramework.Controls.MetroTextBox ContactTxtBx;
        private MetroFramework.Controls.MetroLabel SalaryLbl;
        private MetroFramework.Controls.MetroTextBox SalaryTxtBx;
        private MetroFramework.Controls.MetroLabel AddressLbl;
        private MetroFramework.Controls.MetroTextBox AddressTxtBx;
        private MetroFramework.Controls.MetroButton AddBtn;
        private System.Windows.Forms.ListView listView1;
    }
}

