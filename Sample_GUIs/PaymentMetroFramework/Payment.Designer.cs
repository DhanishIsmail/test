﻿namespace PaymentMetroFramework
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBtn = new MetroFramework.Controls.MetroButton();
            this.CashButton = new MetroFramework.Controls.MetroButton();
            this.CardButton = new MetroFramework.Controls.MetroButton();
            this.CashTextBox = new MetroFramework.Controls.MetroTextBox();
            this.One = new MetroFramework.Controls.MetroButton();
            this.Two = new MetroFramework.Controls.MetroButton();
            this.Three = new MetroFramework.Controls.MetroButton();
            this.Four = new MetroFramework.Controls.MetroButton();
            this.Five = new MetroFramework.Controls.MetroButton();
            this.Six = new MetroFramework.Controls.MetroButton();
            this.Seven = new MetroFramework.Controls.MetroButton();
            this.Eight = new MetroFramework.Controls.MetroButton();
            this.Nine = new MetroFramework.Controls.MetroButton();
            this.Clear = new MetroFramework.Controls.MetroButton();
            this.Zero = new MetroFramework.Controls.MetroButton();
            this.OK = new MetroFramework.Controls.MetroButton();
            this.CardAmountTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.CardAmountLbl = new MetroFramework.Controls.MetroLabel();
            this.ExpDateLbl = new MetroFramework.Controls.MetroLabel();
            this.MonthTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.YearTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.CCVLbl = new MetroFramework.Controls.MetroLabel();
            this.CCVTxtBx = new MetroFramework.Controls.MetroTextBox();
            this.ConfirmBtn = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // CloseBtn
            // 
            this.CloseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseBtn.Location = new System.Drawing.Point(513, 6);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(28, 19);
            this.CloseBtn.Style = MetroFramework.MetroColorStyle.Lime;
            this.CloseBtn.TabIndex = 0;
            this.CloseBtn.Text = "X";
            this.CloseBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // CashButton
            // 
            this.CashButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CashButton.Highlight = true;
            this.CashButton.Location = new System.Drawing.Point(189, 63);
            this.CashButton.Name = "CashButton";
            this.CashButton.Size = new System.Drawing.Size(159, 107);
            this.CashButton.Style = MetroFramework.MetroColorStyle.Lime;
            this.CashButton.TabIndex = 3;
            this.CashButton.Text = "Cash";
            this.CashButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CashButton.Click += new System.EventHandler(this.CashButton_Click);
            // 
            // CardButton
            // 
            this.CardButton.Highlight = true;
            this.CardButton.Location = new System.Drawing.Point(189, 213);
            this.CardButton.Name = "CardButton";
            this.CardButton.Size = new System.Drawing.Size(159, 107);
            this.CardButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.CardButton.TabIndex = 4;
            this.CardButton.Text = "Card";
            this.CardButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CardButton.Click += new System.EventHandler(this.CardButton_Click);
            // 
            // CashTextBox
            // 
            this.CashTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.CashTextBox.Location = new System.Drawing.Point(189, 63);
            this.CashTextBox.Name = "CashTextBox";
            this.CashTextBox.Size = new System.Drawing.Size(159, 23);
            this.CashTextBox.Style = MetroFramework.MetroColorStyle.Lime;
            this.CashTextBox.TabIndex = 5;
            this.CashTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CashTextBox.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CashTextBox.UseStyleColors = true;
            this.CashTextBox.Visible = false;
            // 
            // One
            // 
            this.One.Highlight = true;
            this.One.Location = new System.Drawing.Point(189, 102);
            this.One.Name = "One";
            this.One.Size = new System.Drawing.Size(45, 50);
            this.One.Style = MetroFramework.MetroColorStyle.Lime;
            this.One.TabIndex = 6;
            this.One.Text = "1";
            this.One.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.One.Visible = false;
            this.One.Click += new System.EventHandler(this.One_Click);
            // 
            // Two
            // 
            this.Two.Highlight = true;
            this.Two.Location = new System.Drawing.Point(248, 102);
            this.Two.Name = "Two";
            this.Two.Size = new System.Drawing.Size(45, 50);
            this.Two.Style = MetroFramework.MetroColorStyle.Lime;
            this.Two.TabIndex = 7;
            this.Two.Text = "2";
            this.Two.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Two.Visible = false;
            this.Two.Click += new System.EventHandler(this.Two_Click);
            // 
            // Three
            // 
            this.Three.Highlight = true;
            this.Three.Location = new System.Drawing.Point(303, 102);
            this.Three.Name = "Three";
            this.Three.Size = new System.Drawing.Size(45, 50);
            this.Three.Style = MetroFramework.MetroColorStyle.Lime;
            this.Three.TabIndex = 8;
            this.Three.Text = "3";
            this.Three.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Three.Visible = false;
            this.Three.Click += new System.EventHandler(this.Three_Click);
            // 
            // Four
            // 
            this.Four.Highlight = true;
            this.Four.Location = new System.Drawing.Point(189, 158);
            this.Four.Name = "Four";
            this.Four.Size = new System.Drawing.Size(45, 50);
            this.Four.Style = MetroFramework.MetroColorStyle.Lime;
            this.Four.TabIndex = 9;
            this.Four.Text = "4";
            this.Four.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Four.Visible = false;
            this.Four.Click += new System.EventHandler(this.Four_Click);
            // 
            // Five
            // 
            this.Five.Highlight = true;
            this.Five.Location = new System.Drawing.Point(248, 158);
            this.Five.Name = "Five";
            this.Five.Size = new System.Drawing.Size(45, 50);
            this.Five.Style = MetroFramework.MetroColorStyle.Lime;
            this.Five.TabIndex = 10;
            this.Five.Text = "5";
            this.Five.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Five.Visible = false;
            this.Five.Click += new System.EventHandler(this.Five_Click);
            // 
            // Six
            // 
            this.Six.Highlight = true;
            this.Six.Location = new System.Drawing.Point(303, 158);
            this.Six.Name = "Six";
            this.Six.Size = new System.Drawing.Size(45, 50);
            this.Six.Style = MetroFramework.MetroColorStyle.Lime;
            this.Six.TabIndex = 11;
            this.Six.Text = "6";
            this.Six.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Six.Visible = false;
            this.Six.Click += new System.EventHandler(this.Six_Click);
            // 
            // Seven
            // 
            this.Seven.Highlight = true;
            this.Seven.Location = new System.Drawing.Point(189, 214);
            this.Seven.Name = "Seven";
            this.Seven.Size = new System.Drawing.Size(45, 50);
            this.Seven.Style = MetroFramework.MetroColorStyle.Lime;
            this.Seven.TabIndex = 12;
            this.Seven.Text = "7";
            this.Seven.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Seven.Visible = false;
            this.Seven.Click += new System.EventHandler(this.Seven_Click);
            // 
            // Eight
            // 
            this.Eight.Highlight = true;
            this.Eight.Location = new System.Drawing.Point(248, 214);
            this.Eight.Name = "Eight";
            this.Eight.Size = new System.Drawing.Size(45, 50);
            this.Eight.Style = MetroFramework.MetroColorStyle.Lime;
            this.Eight.TabIndex = 13;
            this.Eight.Text = "8";
            this.Eight.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Eight.Visible = false;
            this.Eight.Click += new System.EventHandler(this.Eight_Click);
            // 
            // Nine
            // 
            this.Nine.Highlight = true;
            this.Nine.Location = new System.Drawing.Point(303, 214);
            this.Nine.Name = "Nine";
            this.Nine.Size = new System.Drawing.Size(45, 50);
            this.Nine.Style = MetroFramework.MetroColorStyle.Lime;
            this.Nine.TabIndex = 14;
            this.Nine.Text = "9";
            this.Nine.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Nine.Visible = false;
            this.Nine.Click += new System.EventHandler(this.Nine_Click);
            // 
            // Clear
            // 
            this.Clear.Highlight = true;
            this.Clear.Location = new System.Drawing.Point(189, 270);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(45, 50);
            this.Clear.Style = MetroFramework.MetroColorStyle.Lime;
            this.Clear.TabIndex = 15;
            this.Clear.Text = "CLR";
            this.Clear.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Clear.Visible = false;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Zero
            // 
            this.Zero.Highlight = true;
            this.Zero.Location = new System.Drawing.Point(248, 270);
            this.Zero.Name = "Zero";
            this.Zero.Size = new System.Drawing.Size(45, 50);
            this.Zero.Style = MetroFramework.MetroColorStyle.Lime;
            this.Zero.TabIndex = 16;
            this.Zero.Text = "0";
            this.Zero.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Zero.Visible = false;
            this.Zero.Click += new System.EventHandler(this.Zero_Click);
            // 
            // OK
            // 
            this.OK.Highlight = true;
            this.OK.Location = new System.Drawing.Point(303, 270);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(45, 50);
            this.OK.Style = MetroFramework.MetroColorStyle.Lime;
            this.OK.TabIndex = 17;
            this.OK.Text = "OK";
            this.OK.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.OK.Visible = false;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // CardAmountTxtBx
            // 
            this.CardAmountTxtBx.Location = new System.Drawing.Point(189, 92);
            this.CardAmountTxtBx.Name = "CardAmountTxtBx";
            this.CardAmountTxtBx.Size = new System.Drawing.Size(159, 23);
            this.CardAmountTxtBx.Style = MetroFramework.MetroColorStyle.Blue;
            this.CardAmountTxtBx.TabIndex = 18;
            this.CardAmountTxtBx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CardAmountTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CardAmountTxtBx.UseStyleColors = true;
            this.CardAmountTxtBx.Visible = false;
            // 
            // CardAmountLbl
            // 
            this.CardAmountLbl.AutoSize = true;
            this.CardAmountLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.CardAmountLbl.Location = new System.Drawing.Point(189, 63);
            this.CardAmountLbl.Name = "CardAmountLbl";
            this.CardAmountLbl.Size = new System.Drawing.Size(59, 19);
            this.CardAmountLbl.TabIndex = 19;
            this.CardAmountLbl.Text = "Amount";
            this.CardAmountLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CardAmountLbl.Visible = false;
            // 
            // ExpDateLbl
            // 
            this.ExpDateLbl.AutoSize = true;
            this.ExpDateLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.ExpDateLbl.Location = new System.Drawing.Point(189, 133);
            this.ExpDateLbl.Name = "ExpDateLbl";
            this.ExpDateLbl.Size = new System.Drawing.Size(129, 38);
            this.ExpDateLbl.TabIndex = 20;
            this.ExpDateLbl.Text = "Exp Date\r\nMonth             Year";
            this.ExpDateLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ExpDateLbl.Visible = false;
            // 
            // MonthTxtBx
            // 
            this.MonthTxtBx.Location = new System.Drawing.Point(189, 176);
            this.MonthTxtBx.Name = "MonthTxtBx";
            this.MonthTxtBx.Size = new System.Drawing.Size(64, 23);
            this.MonthTxtBx.Style = MetroFramework.MetroColorStyle.Blue;
            this.MonthTxtBx.TabIndex = 21;
            this.MonthTxtBx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MonthTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.MonthTxtBx.UseStyleColors = true;
            this.MonthTxtBx.Visible = false;
            // 
            // YearTxtBx
            // 
            this.YearTxtBx.Location = new System.Drawing.Point(284, 176);
            this.YearTxtBx.Name = "YearTxtBx";
            this.YearTxtBx.Size = new System.Drawing.Size(64, 23);
            this.YearTxtBx.Style = MetroFramework.MetroColorStyle.Blue;
            this.YearTxtBx.TabIndex = 22;
            this.YearTxtBx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.YearTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.YearTxtBx.UseStyleColors = true;
            this.YearTxtBx.Visible = false;
            // 
            // CCVLbl
            // 
            this.CCVLbl.AutoSize = true;
            this.CCVLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.CCVLbl.Location = new System.Drawing.Point(189, 214);
            this.CCVLbl.Name = "CCVLbl";
            this.CCVLbl.Size = new System.Drawing.Size(36, 19);
            this.CCVLbl.TabIndex = 23;
            this.CCVLbl.Text = "CCV";
            this.CCVLbl.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CCVLbl.Visible = false;
            // 
            // CCVTxtBx
            // 
            this.CCVTxtBx.Location = new System.Drawing.Point(189, 236);
            this.CCVTxtBx.Name = "CCVTxtBx";
            this.CCVTxtBx.Size = new System.Drawing.Size(64, 23);
            this.CCVTxtBx.Style = MetroFramework.MetroColorStyle.Blue;
            this.CCVTxtBx.TabIndex = 24;
            this.CCVTxtBx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CCVTxtBx.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.CCVTxtBx.UseStyleColors = true;
            this.CCVTxtBx.Visible = false;
            // 
            // ConfirmBtn
            // 
            this.ConfirmBtn.Highlight = true;
            this.ConfirmBtn.Location = new System.Drawing.Point(231, 285);
            this.ConfirmBtn.Name = "ConfirmBtn";
            this.ConfirmBtn.Size = new System.Drawing.Size(75, 23);
            this.ConfirmBtn.Style = MetroFramework.MetroColorStyle.Blue;
            this.ConfirmBtn.TabIndex = 25;
            this.ConfirmBtn.Text = "Confirm";
            this.ConfirmBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ConfirmBtn.Visible = false;
            this.ConfirmBtn.Click += new System.EventHandler(this.ConfirmBtn_Click);
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(542, 390);
            this.ControlBox = false;
            this.Controls.Add(this.ConfirmBtn);
            this.Controls.Add(this.CCVTxtBx);
            this.Controls.Add(this.CCVLbl);
            this.Controls.Add(this.YearTxtBx);
            this.Controls.Add(this.MonthTxtBx);
            this.Controls.Add(this.ExpDateLbl);
            this.Controls.Add(this.CardAmountLbl);
            this.Controls.Add(this.CardAmountTxtBx);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.Zero);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Nine);
            this.Controls.Add(this.Eight);
            this.Controls.Add(this.Seven);
            this.Controls.Add(this.Six);
            this.Controls.Add(this.Five);
            this.Controls.Add(this.Four);
            this.Controls.Add(this.Three);
            this.Controls.Add(this.Two);
            this.Controls.Add(this.One);
            this.Controls.Add(this.CashTextBox);
            this.Controls.Add(this.CardButton);
            this.Controls.Add(this.CashButton);
            this.Controls.Add(this.CloseBtn);
            this.Name = "Payment";
            this.Opacity = 0.98D;
            this.ShadowType = MetroFramework.Forms.MetroForm.MetroFormShadowType.SystemShadow;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Payment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton CloseBtn;
        private MetroFramework.Controls.MetroButton CashButton;
        private MetroFramework.Controls.MetroButton CardButton;
        private MetroFramework.Controls.MetroTextBox CashTextBox;
        private MetroFramework.Controls.MetroButton One;
        private MetroFramework.Controls.MetroButton Two;
        private MetroFramework.Controls.MetroButton Three;
        private MetroFramework.Controls.MetroButton Four;
        private MetroFramework.Controls.MetroButton Five;
        private MetroFramework.Controls.MetroButton Six;
        private MetroFramework.Controls.MetroButton Seven;
        private MetroFramework.Controls.MetroButton Eight;
        private MetroFramework.Controls.MetroButton Nine;
        private MetroFramework.Controls.MetroButton Clear;
        private MetroFramework.Controls.MetroButton Zero;
        private MetroFramework.Controls.MetroButton OK;
        private MetroFramework.Controls.MetroTextBox CardAmountTxtBx;
        private MetroFramework.Controls.MetroLabel CardAmountLbl;
        private MetroFramework.Controls.MetroLabel ExpDateLbl;
        private MetroFramework.Controls.MetroTextBox MonthTxtBx;
        private MetroFramework.Controls.MetroTextBox YearTxtBx;
        private MetroFramework.Controls.MetroLabel CCVLbl;
        private MetroFramework.Controls.MetroTextBox CCVTxtBx;
        private MetroFramework.Controls.MetroButton ConfirmBtn;
    }
}