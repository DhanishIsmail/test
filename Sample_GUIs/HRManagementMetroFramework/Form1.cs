﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace HRManagementMetroFramework
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        private bool EmployeeDetailsLabelClicked = false; private bool EmployeeDetailsVisited = false;
        private bool AddEmployeeLAbelClicked = false; private bool AddEmployeeVisited = false;
        private List<Employee> employees = new List<Employee>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.Columns.Add("ID"); listView1.Columns.Add("Name"); listView1.Columns.Add("NIC"); listView1.Columns.Add("Contact"); listView1.Columns.Add("Expertise"); listView1.Columns.Add("Salary"); listView1.Columns.Add("Address");
        }

        private void ShowAddEmployeeControls(bool visibility)
        {
            NameLbl.Visible = visibility; NameTxtBx.Visible = visibility;
            NICLbl.Visible = visibility; NICTxtBx.Visible = visibility;
            ExpertiseLbl.Visible = visibility; ExpertiseTxtBx.Visible = visibility;
            ContactLbl.Visible = visibility; ContactTxtBx.Visible = visibility;
            SalaryLbl.Visible = visibility; SalaryTxtBx.Visible = visibility;
            AddressLbl.Visible = visibility; AddressTxtBx.Visible = visibility;
            AddBtn.Visible = visibility;
        }

        private void AnimateAddEmployeeControls()
        {
            Transition t = new Transition(new TransitionType_CriticalDamping(1000));
            t.add(NameTxtBx, "Left", this.Width - (100 + NameTxtBx.Width));
            t.add(NameLbl, "Left", this.Width - (100 + NameTxtBx.Width + (NameTxtBx.Location.X - NameLbl.Location.X)));

            t.add(ExpertiseTxtBx, "Left", this.Width - (100 + ExpertiseTxtBx.Width));
            t.add(ExpertiseLbl, "Left", this.Width - (100 + ExpertiseTxtBx.Width + (ExpertiseTxtBx.Location.X - ExpertiseLbl.Location.X)));
            t.add(NICTxtBx, "Left", this.Width - (100 + ExpertiseTxtBx.Width + (ExpertiseTxtBx.Location.X - ExpertiseLbl.Location.X) + (ExpertiseLbl.Location.X - NICTxtBx.Location.X)));
            t.add(NICLbl, "Left", this.Width - (100 + ExpertiseTxtBx.Width + (ExpertiseTxtBx.Location.X - ExpertiseLbl.Location.X) + (ExpertiseLbl.Location.X - NICTxtBx.Location.X) + (NICTxtBx.Location.X - NICLbl.Location.X)));

            t.add(SalaryTxtBx, "Left", this.Width - (100 + SalaryTxtBx.Width));
            t.add(SalaryLbl, "Left", this.Width - (100 + SalaryTxtBx.Width + (SalaryTxtBx.Location.X - SalaryLbl.Location.X)));
            t.add(ContactTxtBx, "Left", this.Width - (100 + SalaryTxtBx.Width + (SalaryTxtBx.Location.X - SalaryLbl.Location.X) + (SalaryLbl.Location.X - ContactTxtBx.Location.X)));
            t.add(ContactLbl, "Left", this.Width - (100 + SalaryTxtBx.Width + (SalaryTxtBx.Location.X - SalaryLbl.Location.X) + (SalaryLbl.Location.X - ContactTxtBx.Location.X) + (ContactTxtBx.Location.X - ContactLbl.Location.X)));

            t.add(AddressTxtBx, "Left", this.Width - (100 + AddressTxtBx.Width));
            t.add(AddressLbl, "Left", this.Width - (100 + AddressTxtBx.Width + (AddressTxtBx.Location.X - AddressLbl.Location.X)));
            t.add(AddBtn, "Left", this.Width - (100 + AddBtn.Width));

            t.run();
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VwEmplyDtlsLbl_MouseEnter(object sender, EventArgs e)
        {

            listView1.Visible = true;
                
            
            if (!EmployeeDetailsVisited)
            {
                Transition t = new Transition(new TransitionType_CriticalDamping(1000));
                t.add(listView1, "Left", this.Width - (100 + listView1.Width));
                t.run();
            }
            if (!EmployeeDetailsLabelClicked)
            {
                VwEmplyDtlsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            }
            EmployeeDetailsVisited = true;
        }

        private void VwEmplyDtlsLbl_MouseLeave(object sender, EventArgs e)
        {
            if (!EmployeeDetailsLabelClicked)
            {
                VwEmplyDtlsLbl.FontSize = MetroFramework.MetroLabelSize.Medium;
                listView1.Visible = false;
            }
            else { }
        }

        private void VwEmplyDtlsLbl_Click(object sender, EventArgs e)
        {
            EmployeeDetailsLabelClicked = true; AddEmployeeLAbelClicked = false;
            ShowAddEmployeeControls(false);
            VwEmplyDtlsLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Style = VwEmplyDtlsLbl.Style;

            AddEmplyLbl.FontSize = MetroFramework.MetroLabelSize.Medium; AddEmplyLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
        }

        private void AddEmplyLbl_MouseEnter(object sender, EventArgs e)
        {

            ShowAddEmployeeControls(true);
                
            
            if (!AddEmployeeVisited)
            {
                AnimateAddEmployeeControls();
            }
            if (!AddEmployeeLAbelClicked)
            {
                AddEmplyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            }
            AddEmployeeVisited = true;
        }

        private void AddEmplyLbl_MouseLeave(object sender, EventArgs e)
        {
            if (!AddEmployeeLAbelClicked)
            {
                AddEmplyLbl.FontSize = MetroFramework.MetroLabelSize.Medium;
                ShowAddEmployeeControls(false);
            }
        }

        private void AddEmplyLbl_Click(object sender, EventArgs e)
        {
            AddEmployeeLAbelClicked = true; EmployeeDetailsLabelClicked = false;
            listView1.Visible = false;
            AddEmplyLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Style = AddEmplyLbl.Style;

            VwEmplyDtlsLbl.FontSize = MetroFramework.MetroLabelSize.Medium; VwEmplyDtlsLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try
            {
                employees.Add(new Employee(employees.Count + 1, NameTxtBx.Text, NICTxtBx.Text, ContactTxtBx.Text, ExpertiseTxtBx.Text, Convert.ToInt32(SalaryTxtBx.Text), AddressTxtBx.Text));
                listView1.Items.Add(new ListViewItem(new string[] { employees.Count.ToString(), NameTxtBx.Text, NICTxtBx.Text, ContactTxtBx.Text, ExpertiseTxtBx.Text, SalaryTxtBx.Text, AddressTxtBx.Text }));
                listView1.Update();
                Message m = new Message(this.Style, "Employee successfully added!");
                m.ShowDialog();
                NameTxtBx.Text = ""; NICTxtBx.Text = ""; ExpertiseTxtBx.Text = ""; ContactTxtBx.Text = ""; SalaryTxtBx.Text = ""; AddressTxtBx.Text = "";
            }
            catch (Exception)
            {
                Message m = new Message(this.Style, "One or more fields are incorrect!");
                m.ShowDialog();
            }
        }

        
    }
}
