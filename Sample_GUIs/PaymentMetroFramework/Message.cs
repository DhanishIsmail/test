﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace PaymentMetroFramework
{
    public partial class Message : MetroFramework.Forms.MetroForm
    {
        private string s = "";
        private MetroColorStyle colorStyle;

        public Message(string message, MetroColorStyle ColorStyle)
        {
            InitializeComponent();
            s = message;
            this.colorStyle = ColorStyle;
        }

        private void Message_Load(object sender, EventArgs e)
        {
            this.Text = s;
            this.Style = colorStyle;
            OK.Style = colorStyle;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
